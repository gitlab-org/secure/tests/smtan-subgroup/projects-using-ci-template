#!/bin/bash

# Check if the correct number of arguments is provided
if [ "$#" -ne 3 ]; then
    echo "Usage: $0 <projects_csv> <destination_directory> <search_string>"
    exit 1
fi

# Assign arguments to variables
PROJECTS_CSV=$1
DESTINATION_DIR=$2
SEARCH_STRING=$3

projects_data=$(tail -n +2 "$PROJECTS_CSV")  # Skip the first line

# Check if destination directory exists
if [ ! -d "$DESTINATION_DIR" ]; then
    echo "Error: Destination directory '$DESTINATION_DIR' not found."
    exit 1
fi

# Create a CSV file to store the matched files
original_dir=$(pwd)
csv_file="$original_dir/matched_files.csv"
echo "project_name,project_url,matched_string,file_path" > "$csv_file"

# Change to destination directory
cd "$DESTINATION_DIR" || exit 1

# Read projects from PROJECTS_CSV
while IFS=',' read -r project_name project_url; do
    echo ""  # Print a newline

    # Replace "https://gitlab.com/" with "git@gitlab.com:" in the project URL
    project_url_git=$(echo "$project_url" | sed 's_https://gitlab.com/_git@gitlab.com:_')
    project_dir=$(basename "$project_url" | cut -d'.' -f1)
    
    # # Clone or pull the project
    # if [ -d "$project_dir" ]; then
    #     echo "Pulling updates for $project_dir"
    #     git -C "$project_dir" pull
    # else
    #     echo "Cloning $project_dir with $project_url_git"
    #     git clone "$project_url_git" "$project_dir"
    # fi

    # Check if any files contain the search string
    echo "Searching $project_dir"
    matched_files=$(grep -Rl --include="*.yml" "$SEARCH_STRING" "$project_dir")
    if [ -n "$matched_files" ]; then
        echo "Some .yml files within $project_dir contain '$SEARCH_STRING':"
        while IFS= read -r matched_file; do
            echo "$project_name,$project_url,$SEARCH_STRING,$matched_file" >> "$csv_file"
        done <<< "$matched_files"
    else
        echo "No matches"
    fi
done <<< "$projects_data"

echo "Matched files saved to $csv_file"
