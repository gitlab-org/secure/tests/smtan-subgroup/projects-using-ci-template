#!/bin/bash

# Check if the correct number of arguments is provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <personal_access_token> <group_id>"
    exit 1
fi

# Assign arguments to variables
PERSONAL_ACCESS_TOKEN=$1
GROUP_ID=$2

# GitLab API base URL
BASE_URL="https://gitlab.com/api/v4"

# Array of subgroup IDs to exclude
EXCLUDED_SUBGROUPS=(
    # https://gitlab.com/gitlab-org/security-products/demos
    3450127
    # https://gitlab.com/gitlab-org/security-products/dependencies
    10301891
    # https://gitlab.com/gitlab-org/security-products/gemnasium
    2626771
    # https://gitlab.com/gitlab-org/security-products/oxeye
    85027458
    # https://gitlab.com/gitlab-org/security-products/rezilion
    87725509
    # https://gitlab.com/gitlab-org/security-products/benchmark-suite
    5715594
    # https://gitlab.com/gitlab-org/security-products/tests
    2504721
)

# Initialize an empty result array
projects=()

# Function to fetch projects in a group
fetch_projects() {
    local group_id=$1
    local page=1
    local per_page=100

    while true; do
        response=$(curl -s --header "PRIVATE-TOKEN: $PERSONAL_ACCESS_TOKEN" "$BASE_URL/groups/$group_id/projects?per_page=$per_page&page=$page")

        if [ -z "$response" ] || [ "$response" == "[]" ]; then
            break
        fi

        projects+=("$response")
        echo "$response" | jq -r '.[].web_url'

        ((page++))
    done
}

# Function to fetch subgroups of a group
fetch_subgroups() {
    local group_id=$1
    local page=1
    local per_page=100

    while true; do
        response=$(curl -s --header "PRIVATE-TOKEN: $PERSONAL_ACCESS_TOKEN" "$BASE_URL/groups/$group_id/subgroups?per_page=$per_page&page=$page")

        if [ -z "$response" ] || [ "$response" == "[]" ]; then
            break
        fi

        subgroups=$(echo "$response" | jq -r '.[].id')
        for subgroup in $subgroups; do
            if [[ ! " ${EXCLUDED_SUBGROUPS[@]} " =~ " $subgroup " ]]; then
                fetch_projects $subgroup
                fetch_subgroups $subgroup
            fi
        done

        ((page++))
    done
}

# Fetch projects in the main group
fetch_projects $GROUP_ID

# Fetch subgroups and their projects recursively
fetch_subgroups $GROUP_ID

# Output the projects to a CSV file
echo "project_name,project_url" > projects.csv
for project in "${projects[@]}"; do
    echo "$project" | jq -r '.[] | "\(.name),\(.web_url)"' >> projects.csv
done

echo "Projects saved to projects.csv"
