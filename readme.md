# Projects Using CI Templates
These scripts can be used to identify secure projects that utilize [ci-templates](https://gitlab.com/gitlab-org/security-products/ci-templates).

This can be particularly useful when we need to make changes to [ci-templates](https://gitlab.com/gitlab-org/security-products/ci-templates) that may impact other projects that rely on it.

# Usage
1. Create a personal access token with `read_api`, `read_repository`.
1. Update the `EXCLUDED_SUBGROUPS` array in `list_projects.sh` if there are other subgroups you may want to exclude or include.
1. Run `list_projects.sh <access_token> <group_id>`
    - 2452873 is the group ID of the secure team
    - This will create a projects.csv containing the project name and project url needed in the next step
    ```
    chmod +x list_projects.sh
    ./list_projects.sh <access_token> 2452873
    ```
1. Run `check.sh <project_csv> <projects_directory> <search_string>`
    - For each of the projects in `project_csv`, it would either clone or pull the latest code into the `projects_directory`
    - It would then search all yml files within the project for the `search_string`
    - If there's a match, it would write the matched string and the file patch to `matched_files.csv`
    ```
    chmod +x list_projects.sh
    ./check.sh projects.csv ./projects `includes-dev/go.yml`
    ```
1. View `matched_files.csv` for all projects and files that matches the search string
1. Once done, delete the <projects_directory> to free up space on your machine